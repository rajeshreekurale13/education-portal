import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAggregateService } from './question-aggregate.service';
import { QuestionService } from '../../entity/question/question.service';

describe('QuestionAggregateService', () => {
  let service: QuestionAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuestionAggregateService,
        {
          provide: QuestionService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuestionAggregateService>(QuestionAggregateService);
  });
  QuestionAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
