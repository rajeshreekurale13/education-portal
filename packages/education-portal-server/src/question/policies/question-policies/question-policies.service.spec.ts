import { Test, TestingModule } from '@nestjs/testing';
import { QuestionPoliciesService } from './question-policies.service';

describe('QuestionPoliciesService', () => {
  let service: QuestionPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionPoliciesService],
    }).compile();

    service = module.get<QuestionPoliciesService>(QuestionPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
