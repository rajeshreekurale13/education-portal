import { ICommand } from '@nestjs/cqrs';
import { UpdateQuestionDto } from '../../entity/question/update-question-dto';

export class UpdateQuestionCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateQuestionDto) {}
}
