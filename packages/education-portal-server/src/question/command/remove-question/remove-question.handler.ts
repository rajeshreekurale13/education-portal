import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveQuestionCommand } from './remove-question.command';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';

@CommandHandler(RemoveQuestionCommand)
export class RemoveQuestionCommandHandler
  implements ICommandHandler<RemoveQuestionCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: QuestionAggregateService,
  ) {}
  async execute(command: RemoveQuestionCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
