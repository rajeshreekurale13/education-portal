import { IEvent } from '@nestjs/cqrs';
import { Question } from '../../entity/question/question.entity';

export class QuestionRemovedEvent implements IEvent {
  constructor(public question: Question) {}
}
