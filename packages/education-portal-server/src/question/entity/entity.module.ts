import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Question } from './question/question.entity';
import { QuestionService } from './question/question.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Question]), CqrsModule],
  providers: [QuestionService],
  exports: [QuestionService],
})
export class QuestionEntitiesModule {}
