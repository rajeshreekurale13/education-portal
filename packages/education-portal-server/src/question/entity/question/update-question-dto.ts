import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
export class UpdateQuestionDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  question: string;

  @IsOptional()
  @IsString()
  question_type: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => UpdateOptionsDto)
  options: UpdateOptionsDto[];
}
export class UpdateOptionsDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  option: string;

  @IsOptional()
  @IsNumber()
  is_correct: number;

  @IsOptional()
  @IsString()
  doctype: string;
}
