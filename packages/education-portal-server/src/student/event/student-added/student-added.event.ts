import { IEvent } from '@nestjs/cqrs';
import { Student } from '../../entity/student/student.entity';

export class StudentAddedEvent implements IEvent {
  constructor(public student: Student, public clientHttpRequest: any) {}
}
