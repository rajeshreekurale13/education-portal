import { ICommand } from '@nestjs/cqrs';

export class RemoveStudentCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
