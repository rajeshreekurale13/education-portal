import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddStudentCommand } from './add-student.command';
import { StudentAggregateService } from '../../aggregates/student-aggregate/student-aggregate.service';

@CommandHandler(AddStudentCommand)
export class AddStudentCommandHandler
  implements ICommandHandler<AddStudentCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StudentAggregateService,
  ) {}
  async execute(command: AddStudentCommand) {
    const { studentPayload: studentPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addStudent(studentPayload, clientHttpRequest);
    aggregate.commit();
  }
}
