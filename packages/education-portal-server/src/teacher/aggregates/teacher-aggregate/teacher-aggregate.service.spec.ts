import { Test, TestingModule } from '@nestjs/testing';
import { TeacherAggregateService } from './teacher-aggregate.service';
describe('teacherAggregateService', () => {
  let service: TeacherAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherAggregateService,
        {
          provide: TeacherAggregateService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TeacherAggregateService>(TeacherAggregateService);
  });
  TeacherAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
