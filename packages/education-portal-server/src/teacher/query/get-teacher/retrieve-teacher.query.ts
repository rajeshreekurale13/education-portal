import { IQuery } from '@nestjs/cqrs';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

export class RetrieveTeacherQuery implements IQuery {
  constructor(
    public readonly uuid: string,
    public readonly req: ClientHttpRequestTokenInterface,
  ) {}
}
