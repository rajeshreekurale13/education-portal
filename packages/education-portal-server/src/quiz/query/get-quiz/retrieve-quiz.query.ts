import { IQuery } from '@nestjs/cqrs';
import { GetCourseQueryDto } from 'src/constants/listing-dto/get-course-dto';

export class RetrieveQuizQuery implements IQuery {
  constructor(
    public readonly getQuizPayload: GetCourseQueryDto,
    public readonly req: any,
  ) {}
}
