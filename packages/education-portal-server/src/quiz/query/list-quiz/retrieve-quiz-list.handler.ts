import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveQuizListQuery } from './retrieve-quiz-list.query';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';

@QueryHandler(RetrieveQuizListQuery)
export class RetrieveQuizListQueryHandler
  implements IQueryHandler<RetrieveQuizListQuery> {
  constructor(private readonly manager: QuizAggregateService) {}
  async execute(query: RetrieveQuizListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getQuizList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
