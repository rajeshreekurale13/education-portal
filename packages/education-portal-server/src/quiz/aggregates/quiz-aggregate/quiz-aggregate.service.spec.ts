import { Test, TestingModule } from '@nestjs/testing';
import { QuizAggregateService } from './quiz-aggregate.service';
import { QuizService } from '../../entity/quiz/quiz.service';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { QuestionService } from '../../../question/entity/question/question.service';

describe('QuizAggregateService', () => {
  let service: QuizAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuizAggregateService,
        {
          provide: QuizService,
          useValue: {},
        },
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: QuestionService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuizAggregateService>(QuizAggregateService);
  });
  QuizAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
