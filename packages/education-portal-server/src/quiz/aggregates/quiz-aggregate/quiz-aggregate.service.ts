import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { QuizDto } from '../../entity/quiz/quiz-dto';
import { Quiz } from '../../entity/quiz/quiz.entity';
import { QuizAddedEvent } from '../../event/quiz-added/quiz-added.event';
import { QuizService } from '../../entity/quiz/quiz.service';
import { QuizRemovedEvent } from '../../event/quiz-removed/quiz-removed.event';
import { QuizUpdatedEvent } from '../../event/quiz-updated/quiz-updated.event';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { QuestionService } from '../../../question/entity/question/question.service';
import {
  TOPIC_NOT_FOUND,
  QUIZ_NOT_FOUND,
  QUESTIONS_NOT_FOUND,
} from '../../../constants/app-strings';

@Injectable()
export class QuizAggregateService extends AggregateRoot {
  constructor(
    private readonly quizService: QuizService,
    private readonly topicService: TopicService,
    private readonly questionService: QuestionService,
  ) {
    super();
  }

  addQuiz(quizPayload: QuizDto, clientHttpRequest) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    quiz.uuid = uuidv4();
    this.apply(new QuizAddedEvent(quiz, clientHttpRequest));
  }

  retrieveQuiz(getQuizPayload: GetCourseQueryDto, req) {
    return from(
      this.topicService.findOne({ name: getQuizPayload.topic_name }),
    ).pipe(
      switchMap(topic => {
        if (!topic) {
          return throwError(new NotFoundException(TOPIC_NOT_FOUND));
        }
        let quizname = '';
        topic.topic_content.forEach(element => {
          if (element.content_type === 'Quiz') {
            quizname = element.content;
          } else {
            quizname = '';
          }
        });
        return from(this.quizService.findOne({ name: quizname })).pipe(
          switchMap(quiz => {
            if (!quiz) {
              return throwError(new NotFoundException(QUIZ_NOT_FOUND));
            }
            const questions: Array<any> = [];
            quiz.question.forEach(eachQuestion => {
              questions.push(eachQuestion.question_link);
            });
            return from(
              this.questionService.find({ name: { $in: questions } }),
            ).pipe(
              switchMap(questionList => {
                if (!questionList) {
                  return throwError(new NotFoundException(QUESTIONS_NOT_FOUND));
                }
                return of(questionList);
              }),
            );
          }),
        );
      }),
    );
  }

  async getQuizList(offset, limit, sort, search, clientHttpRequest) {
    return await this.quizService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.quizService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new QuizRemovedEvent(found));
  }

  async update(updatePayload: UpdateQuizDto) {
    const provider = await this.quizService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new QuizUpdatedEvent(update));
  }
}
