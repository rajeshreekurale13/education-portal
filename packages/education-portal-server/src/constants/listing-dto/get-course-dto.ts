import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class GetCourseQueryDto {
  @IsNotEmpty()
  program_name: string;

  @IsNotEmpty()
  course_name: string;

  @IsOptional()
  @IsString()
  topic_name: string;
}
