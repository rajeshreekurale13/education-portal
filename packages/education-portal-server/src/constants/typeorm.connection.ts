import {
  ConfigService,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
  MONGO_URI_PREFIX,
  CACHE_DB_USER,
  CACHE_DB_PASSWORD,
  CACHE_DB_NAME,
} from '../config/config.service';
import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import { ServerSettings } from '../system-settings/entities/server-settings/server-settings.entity';
import { TokenCache } from '../auth/entities/token-cache/token-cache.entity';
import { FrappeToken } from '../direct/entities/frappe-token/frappe-token.entity';
import { RequestState } from '../direct/entities/request-state/request-state.entity';
import { Teacher } from '../teacher/entity/teacher/teacher.entity';
import { Student } from '../student/entity/student/student.entity';
import { Program } from '../program/entity/program/program.entity';
import { Course } from '../course/entity/course/course.entity';
import { Topic } from '../topic/entity/topic/topic.entity';
import { Video } from '../video/entity/video/video.entity';
import { Article } from '../article/entity/article/article.entity';
import { Quiz } from '../quiz/entity/quiz/quiz.entity';
import { Question } from '../question/entity/question/question.entity';
import { Department } from '../department/entity/department/department.entity';
import { Attendance } from '../attendance/entity/attendance/attendance.entity';
import { StudentGroup } from '../student-group/entity/student-group/student-group.entity';
import { Room } from '../room/entity/room/room.entity';
import { CourseSchedule } from '../course-schedule/entity/course-schedule/course-schedule.entity';

export const TOKEN_CACHE_CONNECTION = 'tokencache';
export const DEFAULT = 'default';

export function connectTypeORM(config: ConfigService): MongoConnectionOptions {
  const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';
  const mongoOptions = 'retryWrites=true';
  return {
    name: DEFAULT,
    url: `${mongoUriPrefix}://${config.get(DB_USER)}:${config.get(
      DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}?${mongoOptions}`,
    type: 'mongodb',
    logging: false,
    synchronize: true,
    entities: [
      ServerSettings,
      FrappeToken,
      RequestState,
      Teacher,
      Student,
      Program,
      Course,
      Topic,
      Video,
      Article,
      Quiz,
      Question,
      Department,
      Attendance,
      StudentGroup,
      Room,
      CourseSchedule,
    ],
    useNewUrlParser: true,
    w: 'majority',
    useUnifiedTopology: true,
  };
}

export function connectTypeORMTokenCache(
  config: ConfigService,
): MongoConnectionOptions {
  const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';
  const mongoOptions = 'retryWrites=true';
  return {
    name: TOKEN_CACHE_CONNECTION,
    url: `${mongoUriPrefix}://${config.get(CACHE_DB_USER)}:${config.get(
      CACHE_DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(CACHE_DB_NAME)}?${mongoOptions}`,
    type: 'mongodb',
    logging: false,
    synchronize: true,
    entities: [TokenCache],
    useNewUrlParser: true,
    w: 'majority',
    useUnifiedTopology: true,
  };
}
