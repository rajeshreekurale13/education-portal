import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveAttendanceQuery } from './retrieve-attendance.query';
import { AttendanceAggregateService } from '../../aggregates/attendance-aggregate/attendance-aggregate.service';

@QueryHandler(RetrieveAttendanceQuery)
export class RetrieveAttendanceQueryHandler
  implements IQueryHandler<RetrieveAttendanceQuery> {
  constructor(private readonly manager: AttendanceAggregateService) {}

  async execute(query: RetrieveAttendanceQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveAttendance(uuid, req);
  }
}
