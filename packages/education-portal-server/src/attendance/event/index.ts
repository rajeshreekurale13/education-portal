import { AttendanceAddedCommandHandler } from './attendance-added/attendance-added.handler';
import { AttendanceRemovedCommandHandler } from './attendance-removed/attendance-removed.handler';
import { AttendanceUpdatedCommandHandler } from './attendance-updated/attendance-updated.handler';

export const AttendanceEventManager = [
  AttendanceAddedCommandHandler,
  AttendanceRemovedCommandHandler,
  AttendanceUpdatedCommandHandler,
];
