import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AttendanceUpdatedEvent } from './attendance-updated.event';
import { AttendanceService } from '../../entity/attendance/attendance.service';

@EventsHandler(AttendanceUpdatedEvent)
export class AttendanceUpdatedCommandHandler
  implements IEventHandler<AttendanceUpdatedEvent> {
  constructor(private readonly object: AttendanceService) {}

  async handle(event: AttendanceUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
