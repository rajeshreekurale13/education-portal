import { Injectable, BadRequestException } from '@nestjs/common';
import { StudentGroupWebhookDto } from '../../entity/student-group/student-group-webhook-dto';
import { StudentGroupService } from '../../../student-group/entity/student-group/student-group.service';
import { StudentGroup } from '../../../student-group/entity/student-group/student-group.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { STUDENT_GROUP_ALREADY_EXISTS } from '../../../constants/messages';

import * as uuidv4 from 'uuid/v4';
@Injectable()
export class StudentGroupWebhookAggregateService {
  constructor(private readonly studentGroupService: StudentGroupService) {}

  studentGroupCreated(studentGroupPayload: StudentGroupWebhookDto) {
    return from(
      this.studentGroupService.findOne({
        name: studentGroupPayload.name,
      }),
    ).pipe(
      switchMap(studentGroup => {
        if (studentGroup) {
          return throwError(
            new BadRequestException(STUDENT_GROUP_ALREADY_EXISTS),
          );
        }
        const provider = this.mapStudentGroup(studentGroupPayload);
        return from(this.studentGroupService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapStudentGroup(studentGroupPayload: StudentGroupWebhookDto) {
    const studentGroup = new StudentGroup();
    Object.assign(studentGroup, studentGroupPayload);
    studentGroup.uuid = uuidv4();
    studentGroup.isSynced = true;
    return studentGroup;
  }

  studentGroupDeleted(studentGroupPayload: StudentGroupWebhookDto) {
    this.studentGroupService.deleteOne({ name: studentGroupPayload.name });
  }

  studentGroupUpdated(studentGroupPayload: StudentGroupWebhookDto) {
    return from(
      this.studentGroupService.findOne({ name: studentGroupPayload.name }),
    ).pipe(
      switchMap(studentGroup => {
        if (!studentGroup) {
          return this.studentGroupCreated(studentGroupPayload);
        }
        studentGroup.isSynced = true;

        return from(
          this.studentGroupService.updateOne(
            { name: studentGroup.name },
            { $set: studentGroupPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
