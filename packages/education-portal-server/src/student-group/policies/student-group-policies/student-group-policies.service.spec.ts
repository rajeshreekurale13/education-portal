import { Test, TestingModule } from '@nestjs/testing';
import { StudentGroupPoliciesService } from './student-group-policies.service';

describe('StudentGroupPoliciesService', () => {
  let service: StudentGroupPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentGroupPoliciesService],
    }).compile();

    service = module.get<StudentGroupPoliciesService>(
      StudentGroupPoliciesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
