import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { StudentGroupWebhookDto } from '../../entity/student-group/student-group-webhook-dto';
import { StudentGroupWebhookAggregateService } from '../../aggregates/student-group-webhook-aggregate/student-group-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('student_group')
export class StudentGroupWebhookController {
  constructor(
    private readonly studentGroupWebhookAggreagte: StudentGroupWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentGroupCreated(@Body() studentGroupPayload: StudentGroupWebhookDto) {
    return this.studentGroupWebhookAggreagte.studentGroupCreated(
      studentGroupPayload,
    );
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentGroupUpdated(@Body() studentGroupPayload: StudentGroupWebhookDto) {
    return this.studentGroupWebhookAggreagte.studentGroupUpdated(
      studentGroupPayload,
    );
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentGroupDeleted(@Body() studentGroupPayload: StudentGroupWebhookDto) {
    return this.studentGroupWebhookAggreagte.studentGroupDeleted(
      studentGroupPayload,
    );
  }
}
