import { IEvent } from '@nestjs/cqrs';
import { StudentGroup } from '../../entity/student-group/student-group.entity';

export class StudentGroupUpdatedEvent implements IEvent {
  constructor(public updatePayload: StudentGroup) {}
}
