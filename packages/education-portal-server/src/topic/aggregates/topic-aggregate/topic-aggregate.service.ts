import {
  Injectable,
  NotFoundException,
  HttpService,
  NotImplementedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { TopicDto } from '../../entity/topic/topic-dto';
import { Topic } from '../../entity/topic/topic.entity';
import { TopicAddedEvent } from '../../event/topic-added/topic-added.event';
import { TopicService } from '../../entity/topic/topic.service';
import { TopicRemovedEvent } from '../../event/topic-removed/topic-removed.event';
import { TopicUpdatedEvent } from '../../event/topic-updated/topic-updated.event';
import { UpdateTopicDto } from '../../entity/topic/update-topic-dto';
import { throwError } from 'rxjs';
import { switchMap, map, catchError, retry } from 'rxjs/operators';
import {
  FRAPPE_API_GET_TOPIC_ENDPOINT,
  FRAPPE_API_GET_COURSE_ENDPOINT,
} from '../../../constants/routes';
import {
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  APPLICATION_JSON_CONTENT_TYPE,
  ACCEPT,
} from '../../../constants/app-strings';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { UpdateCourseTopicsDto } from '../../../course/entity/course/update-course-dto';

@Injectable()
export class TopicAggregateService extends AggregateRoot {
  constructor(
    private readonly topicService: TopicService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
  ) {
    super();
  }

  addTopic(topicPayload: TopicDto, clientHttpRequest) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    topic.uuid = uuidv4();
    this.apply(new TopicAddedEvent(topic, clientHttpRequest));
  }

  async retrieveTopic(uuid: string, req) {
    const provider = await this.topicService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getTopicList(offset, limit, sort, search, clientHttpRequest) {
    return await this.topicService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.topicService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new TopicRemovedEvent(found));
  }

  async update(updatePayload: UpdateTopicDto) {
    const provider = await this.topicService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new TopicUpdatedEvent(update));
  }

  addExtraTopic(
    topic_name: UpdateCourseTopicsDto,
    course_name: string,
    clientHttpRequest,
  ) {
    return this.getExistingTopic(topic_name, course_name, clientHttpRequest);
  }

  getExistingTopic(
    topic_name: UpdateCourseTopicsDto,
    course_name: string,
    clientHttpRequest,
  ) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const getUrl =
          settings.authServerURL +
          FRAPPE_API_GET_TOPIC_ENDPOINT +
          topic_name.topic_name;
        return this.http
          .get(getUrl, {
            headers: {
              [AUTHORIZATION]:
                BEARER_HEADER_VALUE_PREFIX +
                clientHttpRequest.token.accessToken,
            },
          })
          .pipe(
            map(res => res.data.data),
            switchMap(() => {
              return this.addCourseTopic(
                topic_name,
                course_name,
                clientHttpRequest,
              );
            }),
            catchError(err =>
              this.addNewTopic(topic_name, course_name, clientHttpRequest),
            ),
          );
      }),
    );
  }

  addNewTopic(
    topic_name: UpdateCourseTopicsDto,
    course_name: string,
    clientHttpRequest,
  ) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const url = settings.authServerURL + FRAPPE_API_GET_TOPIC_ENDPOINT;
        return this.http
          .post(
            url,
            { topic_name: topic_name.topic_name },
            {
              headers: {
                [AUTHORIZATION]:
                  BEARER_HEADER_VALUE_PREFIX +
                  clientHttpRequest.token.accessToken,
                [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
              },
            },
          )
          .pipe(
            map(res => res.data.data),
            switchMap(() => {
              return this.addCourseTopic(
                topic_name,
                course_name,
                clientHttpRequest,
              );
            }),
          );
      }),
      retry(3),
    );
  }

  addCourseTopic(
    topic_name: UpdateCourseTopicsDto,
    course_name: string,
    clientHttpRequest,
  ) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const getUrl =
          settings.authServerURL + FRAPPE_API_GET_COURSE_ENDPOINT + course_name;
        return this.http
          .get(getUrl, {
            headers: {
              [AUTHORIZATION]:
                BEARER_HEADER_VALUE_PREFIX +
                clientHttpRequest.token.accessToken,
            },
          })
          .pipe(
            map(res => res.data),
            switchMap(getCourseData => {
              getCourseData.data.topics.push({
                topic: topic_name.topic_name,
                topic_name: topic_name.topic_name,
              });
              const body = {
                topics: getCourseData.data.topics,
              };

              return this.http.put(getUrl, body, {
                headers: {
                  [AUTHORIZATION]:
                    BEARER_HEADER_VALUE_PREFIX +
                    clientHttpRequest.token.accessToken,
                  [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
                  [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
                },
              });
            }),
          )
          .pipe(map(res => res.data.data));
      }),
    );
  }
}
