import { Test, TestingModule } from '@nestjs/testing';
import { TopicWebhookAggregateService } from './topic-webhook-aggregate.service';
import { TopicService } from '../../entity/topic/topic.service';

describe('TopicWebhookAggregateService', () => {
  let service: TopicWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicWebhookAggregateService,
        {
          provide: TopicService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicWebhookAggregateService>(
      TopicWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
