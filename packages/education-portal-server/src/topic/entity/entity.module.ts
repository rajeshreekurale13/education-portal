import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Topic } from './topic/topic.entity';
import { TopicService } from './topic/topic.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Topic]), CqrsModule],
  providers: [TopicService],
  exports: [TopicService],
})
export class TopicEntitiesModule {}
