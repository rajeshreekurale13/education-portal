import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TopicUpdatedEvent } from './topic-updated.event';
import { TopicService } from '../../entity/topic/topic.service';

@EventsHandler(TopicUpdatedEvent)
export class TopicUpdatedCommandHandler
  implements IEventHandler<TopicUpdatedEvent> {
  constructor(private readonly object: TopicService) {}

  async handle(event: TopicUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
