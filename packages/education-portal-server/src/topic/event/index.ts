import { TopicAddedCommandHandler } from './topic-added/topic-added.handler';
import { TopicRemovedCommandHandler } from './topic-removed/topic-removed.handler';
import { TopicUpdatedCommandHandler } from './topic-updated/topic-updated.handler';

export const TopicEventManager = [
  TopicAddedCommandHandler,
  TopicRemovedCommandHandler,
  TopicUpdatedCommandHandler,
];
