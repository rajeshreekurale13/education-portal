import { IEvent } from '@nestjs/cqrs';
import { Topic } from '../../entity/topic/topic.entity';

export class TopicAddedEvent implements IEvent {
  constructor(public topic: Topic, public clientHttpRequest: any) {}
}
