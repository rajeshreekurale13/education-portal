import { RetrieveTopicQueryHandler } from './get-topic/retrieve-topic.handler';
import { RetrieveTopicListQueryHandler } from './list-topic/retrieve-topic-list.handler';

export const TopicQueryManager = [
  RetrieveTopicQueryHandler,
  RetrieveTopicListQueryHandler,
];
