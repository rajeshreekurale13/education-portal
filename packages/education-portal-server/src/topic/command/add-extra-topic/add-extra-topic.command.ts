import { ICommand } from '@nestjs/cqrs';
import { UpdateCourseTopicsDto } from '../../../course/entity/course/update-course-dto';

export class AddExtraTopicCommand implements ICommand {
  constructor(
    public readonly topic_name: UpdateCourseTopicsDto,
    public readonly course_name: string,
    public readonly clientHttpRequest: any,
  ) {}
}
