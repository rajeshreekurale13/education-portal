import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddExtraTopicCommand } from './add-extra-topic.command';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(AddExtraTopicCommand)
export class AddExtraTopicCommandHandler
  implements ICommandHandler<AddExtraTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}
  async execute(command: AddExtraTopicCommand) {
    const { topic_name, course_name, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .addExtraTopic(topic_name, course_name, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
