import { AddTopicCommandHandler } from './add-topic/add-topic.handler';
import { RemoveTopicCommandHandler } from './remove-topic/remove-topic.handler';
import { UpdateTopicCommandHandler } from './update-topic/update-topic.handler';
import { AddExtraTopicCommandHandler } from './add-extra-topic/add-extra-topic.handler';

export const TopicCommandManager = [
  AddTopicCommandHandler,
  RemoveTopicCommandHandler,
  UpdateTopicCommandHandler,
  AddExtraTopicCommandHandler,
];
