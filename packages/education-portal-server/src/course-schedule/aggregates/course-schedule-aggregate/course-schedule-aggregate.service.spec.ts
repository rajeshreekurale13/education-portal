import { Test, TestingModule } from '@nestjs/testing';
import { CourseScheduleAggregateService } from './course-schedule-aggregate.service';
import { CourseScheduleService } from '../../entity/course-schedule/course-schedule.service';

describe('CourseScheduleAggregateService', () => {
  let service: CourseScheduleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseScheduleAggregateService,
        {
          provide: CourseScheduleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CourseScheduleAggregateService>(
      CourseScheduleAggregateService,
    );
  });
  CourseScheduleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
