import { Injectable, BadRequestException } from '@nestjs/common';
import { CourseScheduleWebhookDto } from '../../entity/course-schedule/course-schedule-webhook-dto';
import { CourseScheduleService } from '../../../course-schedule/entity/course-schedule/course-schedule.service';
import { CourseSchedule } from '../../../course-schedule/entity/course-schedule/course-schedule.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { COURSE_SCHEDULE_ALREADY_EXISTS } from '../../../constants/messages';

import * as uuidv4 from 'uuid/v4';
@Injectable()
export class CourseScheduleWebhookAggregateService {
  constructor(private readonly courseScheduleService: CourseScheduleService) {}

  courseScheduleCreated(courseSchedulePayload: CourseScheduleWebhookDto) {
    return from(
      this.courseScheduleService.findOne({
        name: courseSchedulePayload.name,
      }),
    ).pipe(
      switchMap(courseSchedule => {
        if (courseSchedule) {
          return throwError(
            new BadRequestException(COURSE_SCHEDULE_ALREADY_EXISTS),
          );
        }
        const provider = this.mapCourseSchedule(courseSchedulePayload);
        return from(this.courseScheduleService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapCourseSchedule(courseSchedulePayload: CourseScheduleWebhookDto) {
    const courseSchedule = new CourseSchedule();
    Object.assign(courseSchedule, courseSchedulePayload);
    courseSchedule.uuid = uuidv4();
    courseSchedule.isSynced = true;
    return courseSchedule;
  }

  courseScheduleDeleted(courseSchedulePayload: CourseScheduleWebhookDto) {
    this.courseScheduleService.deleteOne({ name: courseSchedulePayload.name });
  }

  courseScheduleUpdated(courseSchedulePayload: CourseScheduleWebhookDto) {
    return from(
      this.courseScheduleService.findOne({ name: courseSchedulePayload.name }),
    ).pipe(
      switchMap(courseSchedule => {
        if (!courseSchedule) {
          return this.courseScheduleCreated(courseSchedulePayload);
        }
        courseSchedule.isSynced = true;
        return from(
          this.courseScheduleService.updateOne(
            { name: courseSchedule.name },
            { $set: courseSchedulePayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
