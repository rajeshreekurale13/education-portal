import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class DepartmentWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  department_name: string;

  @IsOptional()
  @IsString()
  parent_department: string;

  @IsOptional()
  @IsString()
  company: string;

  @IsOptional()
  @IsNumber()
  is_group: number;

  @IsOptional()
  @IsNumber()
  disabled: number;

  @IsOptional()
  @IsNumber()
  lft: number;

  @IsOptional()
  @IsNumber()
  rgt: number;

  @IsOptional()
  @IsString()
  old_parent: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => ApproversWebookDto)
  leave_approvers: ApproversWebookDto[];

  @ValidateNested()
  @Type(() => ApproversWebookDto)
  expense_approvers: ApproversWebookDto[];
}

export class ApproversWebookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  approver: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
