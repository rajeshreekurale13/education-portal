import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveDepartmentListQuery } from './retrieve-department-list.query';
import { DepartmentAggregateService } from '../../aggregates/department-aggregate/department-aggregate.service';

@QueryHandler(RetrieveDepartmentListQuery)
export class RetrieveDepartmentListQueryHandler
  implements IQueryHandler<RetrieveDepartmentListQuery> {
  constructor(private readonly manager: DepartmentAggregateService) {}
  async execute(query: RetrieveDepartmentListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getDepartmentList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
