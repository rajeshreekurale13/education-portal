import { ICommand } from '@nestjs/cqrs';

export class RemoveDepartmentCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
