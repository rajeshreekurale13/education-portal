import { ICommand } from '@nestjs/cqrs';
import { UpdateDepartmentDto } from '../../entity/department/update-department-dto';

export class UpdateDepartmentCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateDepartmentDto) {}
}
