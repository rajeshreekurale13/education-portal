import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddDepartmentCommand } from './add-department.command';
import { DepartmentAggregateService } from '../../aggregates/department-aggregate/department-aggregate.service';

@CommandHandler(AddDepartmentCommand)
export class AddDepartmentCommandHandler
  implements ICommandHandler<AddDepartmentCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: DepartmentAggregateService,
  ) {}
  async execute(command: AddDepartmentCommand) {
    const { departmentPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addDepartment(departmentPayload, clientHttpRequest);
    aggregate.commit();
  }
}
