import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentPoliciesService } from './department-policies.service';

describe('DepartmentPoliciesService', () => {
  let service: DepartmentPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DepartmentPoliciesService],
    }).compile();

    service = module.get<DepartmentPoliciesService>(DepartmentPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
