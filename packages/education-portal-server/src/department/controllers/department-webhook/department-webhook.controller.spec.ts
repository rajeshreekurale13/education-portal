import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentWebhookController } from './department-webhook.controller';
import { DepartmentWebhookAggregateService } from '../../../department/aggregates/department-webhook-aggregate/department-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('DepartmentWebhook Controller', () => {
  let controller: DepartmentWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: DepartmentWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [DepartmentWebhookController],
    }).compile();

    controller = module.get<DepartmentWebhookController>(
      DepartmentWebhookController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
