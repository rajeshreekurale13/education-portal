import { RetrieveRoomQueryHandler } from './get-room/retrieve-room.handler';
import { RetrieveRoomListQueryHandler } from './list-room/retrieve-room-list.handler';

export const RoomQueryManager = [
  RetrieveRoomQueryHandler,
  RetrieveRoomListQueryHandler,
];
