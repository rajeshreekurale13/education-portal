import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddRoomCommand } from './add-room.command';
import { RoomAggregateService } from '../../aggregates/room-aggregate/room-aggregate.service';

@CommandHandler(AddRoomCommand)
export class AddRoomCommandHandler implements ICommandHandler<AddRoomCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: RoomAggregateService,
  ) {}
  async execute(command: AddRoomCommand) {
    const { roomPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addRoom(roomPayload, clientHttpRequest);
    aggregate.commit();
  }
}
