import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Room extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  room_name: string;

  @Column()
  room_number: string;

  @Column()
  seating_capacity: string;

  @Column()
  doctype: string;

  @Column()
  isSynced: boolean;
}
