import { IsNotEmpty, IsOptional, IsString, IsNumber } from 'class-validator';
export class UpdateRoomDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  room_name: string;

  @IsOptional()
  @IsString()
  room_number: string;

  @IsOptional()
  @IsString()
  seating_capacity: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
