import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Room } from './room/room.entity';
import { RoomService } from './room/room.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Room]), CqrsModule],
  providers: [RoomService],
  exports: [RoomService],
})
export class RoomEntitiesModule {}
