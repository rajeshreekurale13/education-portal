import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { RoomWebhookDto } from '../../entity/room/room-webhook-dto';
import { RoomWebhookAggregateService } from '../../aggregates/room-webhook-aggregate/room-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('room')
export class RoomWebhookController {
  constructor(
    private readonly roomWebhookAggreagte: RoomWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  roomCreated(@Body() roomPayload: RoomWebhookDto) {
    return this.roomWebhookAggreagte.roomCreated(roomPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  roomUpdated(@Body() roomPayload: RoomWebhookDto) {
    return this.roomWebhookAggreagte.roomUpdated(roomPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  roomDeleted(@Body() roomPayload: RoomWebhookDto) {
    return this.roomWebhookAggreagte.roomDeleted(roomPayload);
  }
}
