import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RoomDto } from '../../entity/room/room-dto';
import { AddRoomCommand } from '../../command/add-room/add-room.command';
import { RemoveRoomCommand } from '../../command/remove-room/remove-room.command';
import { UpdateRoomCommand } from '../../command/update-room/update-room.command';
import { RetrieveRoomQuery } from '../../query/get-room/retrieve-room.query';
import { RetrieveRoomListQuery } from '../../query/list-room/retrieve-room-list.query';
import { UpdateRoomDto } from '../../entity/room/update-room-dto';

@Controller('room')
export class RoomController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() roomPayload: RoomDto, @Req() req) {
    return this.commandBus.execute(new AddRoomCommand(roomPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveRoomCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveRoomQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveRoomListQuery(offset, limit, sort, search, clientHttpRequest),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateRoomDto) {
    return this.commandBus.execute(new UpdateRoomCommand(updatePayload));
  }
}
