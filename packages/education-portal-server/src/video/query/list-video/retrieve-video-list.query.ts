import { IQuery } from '@nestjs/cqrs';

export class RetrieveVideoListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public clientHttpRequest: any,
  ) {}
}
