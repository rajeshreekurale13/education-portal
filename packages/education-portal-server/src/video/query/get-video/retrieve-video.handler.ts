import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveVideoQuery } from './retrieve-video.query';
import { VideoAggregateService } from '../../aggregates/video-aggregate/video-aggregate.service';

@QueryHandler(RetrieveVideoQuery)
export class RetrieveVideoQueryHandler
  implements IQueryHandler<RetrieveVideoQuery> {
  constructor(private readonly manager: VideoAggregateService) {}

  async execute(query: RetrieveVideoQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveVideo(uuid, req);
  }
}
