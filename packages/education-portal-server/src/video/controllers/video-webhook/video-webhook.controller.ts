import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { VideoWebhookDto } from '../../entity/video/video-webhook-dto';
import { VideoWebhookAggregateService } from '../../aggregates/video-webhook-aggregate/video-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('video')
export class VideoWebhookController {
  constructor(
    private readonly videoWebhookAggreagte: VideoWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  videoCreated(@Body() videoPayload: VideoWebhookDto) {
    return this.videoWebhookAggreagte.videoCreated(videoPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  videoUpdated(@Body() videoPayload: VideoWebhookDto) {
    return this.videoWebhookAggreagte.videoUpdated(videoPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  videoDeleted(@Body() videoPayload: VideoWebhookDto) {
    return this.videoWebhookAggreagte.videoDeleted(videoPayload);
  }
}
