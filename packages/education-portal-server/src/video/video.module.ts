import { Module, HttpModule } from '@nestjs/common';
import { VideoAggregatesManager } from './aggregates';
import { VideoEntitiesModule } from './entity/entity.module';
import { VideoQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { VideoCommandManager } from './command';
import { VideoEventManager } from './event';
import { VideoController } from './controllers/video/video.controller';
import { VideoPoliciesService } from './policies/video-policies/video-policies.service';
import { VideoWebhookController } from './controllers/video-webhook/video-webhook.controller';

@Module({
  imports: [VideoEntitiesModule, CqrsModule, HttpModule],
  controllers: [VideoController, VideoWebhookController],
  providers: [
    ...VideoAggregatesManager,
    ...VideoQueryManager,
    ...VideoEventManager,
    ...VideoCommandManager,
    VideoPoliciesService,
  ],
  exports: [VideoEntitiesModule],
})
export class VideoModule {}
