import { IEvent } from '@nestjs/cqrs';
import { Video } from '../../entity/video/video.entity';

export class VideoUpdatedEvent implements IEvent {
  constructor(public updatePayload: Video) {}
}
