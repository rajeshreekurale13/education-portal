import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { VideoUpdatedEvent } from './video-updated.event';
import { VideoService } from '../../entity/video/video.service';

@EventsHandler(VideoUpdatedEvent)
export class VideoUpdatedCommandHandler
  implements IEventHandler<VideoUpdatedEvent> {
  constructor(private readonly object: VideoService) {}

  async handle(event: VideoUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
