import { IEvent } from '@nestjs/cqrs';
import { Video } from '../../entity/video/video.entity';

export class VideoRemovedEvent implements IEvent {
  constructor(public video: Video) {}
}
