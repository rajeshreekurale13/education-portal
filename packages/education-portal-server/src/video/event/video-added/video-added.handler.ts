import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { VideoAddedEvent } from './video-added.event';
import { VideoService } from '../../entity/video/video.service';

@EventsHandler(VideoAddedEvent)
export class VideoAddedCommandHandler
  implements IEventHandler<VideoAddedEvent> {
  constructor(private readonly videoService: VideoService) {}
  async handle(event: VideoAddedEvent) {
    const { video } = event;
    await this.videoService.create(video);
  }
}
