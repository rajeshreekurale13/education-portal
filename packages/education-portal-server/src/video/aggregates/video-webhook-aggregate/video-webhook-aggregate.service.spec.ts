import { Test, TestingModule } from '@nestjs/testing';
import { VideoWebhookAggregateService } from './video-webhook-aggregate.service';
import { VideoService } from '../../../video/entity/video/video.service';

describe('VideoWebhookAggregateService', () => {
  let service: VideoWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VideoWebhookAggregateService,
        {
          provide: VideoService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<VideoWebhookAggregateService>(
      VideoWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
