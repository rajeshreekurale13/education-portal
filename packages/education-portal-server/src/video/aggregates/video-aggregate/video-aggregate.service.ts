import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { VideoDto } from '../../entity/video/video-dto';
import { Video } from '../../entity/video/video.entity';
import { VideoAddedEvent } from '../../event/video-added/video-added.event';
import { VideoService } from '../../entity/video/video.service';
import { VideoRemovedEvent } from '../../event/video-removed/video-removed.event';
import { VideoUpdatedEvent } from '../../event/video-updated/video-updated.event';
import { UpdateVideoDto } from '../../entity/video/update-video-dto';

@Injectable()
export class VideoAggregateService extends AggregateRoot {
  constructor(private readonly videoService: VideoService) {
    super();
  }

  addVideo(videoPayload: VideoDto, clientHttpRequest) {
    const video = new Video();
    Object.assign(video, videoPayload);
    video.uuid = uuidv4();
    this.apply(new VideoAddedEvent(video, clientHttpRequest));
  }

  async retrieveVideo(uuid: string, req) {
    const provider = await this.videoService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getVideoList(offset, limit, sort, search, clientHttpRequest) {
    return await this.videoService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.videoService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new VideoRemovedEvent(found));
  }

  async update(updatePayload: UpdateVideoDto) {
    const provider = await this.videoService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new VideoUpdatedEvent(update));
  }
}
