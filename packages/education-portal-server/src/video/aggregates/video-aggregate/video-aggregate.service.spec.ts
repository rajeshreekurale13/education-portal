import { Test, TestingModule } from '@nestjs/testing';
import { VideoAggregateService } from './video-aggregate.service';
import { VideoService } from '../../entity/video/video.service';

describe('VideoAggregateService', () => {
  let service: VideoAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VideoAggregateService,
        {
          provide: VideoService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<VideoAggregateService>(VideoAggregateService);
  });
  VideoAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
