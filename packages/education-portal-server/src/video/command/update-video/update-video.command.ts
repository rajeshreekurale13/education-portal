import { ICommand } from '@nestjs/cqrs';
import { UpdateVideoDto } from '../../entity/video/update-video-dto';

export class UpdateVideoCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateVideoDto) {}
}
