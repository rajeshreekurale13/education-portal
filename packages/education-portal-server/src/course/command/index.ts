import { AddCourseCommandHandler } from './add-course/add-course.handler';
import { RemoveCourseCommandHandler } from './remove-course/remove-course.handler';
import { UpdateCourseCommandHandler } from './update-course/update-course.handler';
import { UpdateTopicsStatusCommandHandler } from './update-course-topic/update-course-topic.handler';

export const CourseCommandManager = [
  AddCourseCommandHandler,
  RemoveCourseCommandHandler,
  UpdateCourseCommandHandler,
  UpdateTopicsStatusCommandHandler,
];
