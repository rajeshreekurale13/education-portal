import { ICommand } from '@nestjs/cqrs';
import { UpdateTopicStatusDto } from '../../entity/course/update-topic-status-dto';
export class UpdateTopicStatusCommand implements ICommand {
  constructor(
    public readonly updateTopicStatusPayload: UpdateTopicStatusDto,
    public readonly req,
  ) {}
}
