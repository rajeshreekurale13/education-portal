import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';
import { UpdateTopicStatusCommand } from './update-course-topic.command';

@CommandHandler(UpdateTopicStatusCommand)
export class UpdateTopicsStatusCommandHandler
  implements ICommandHandler<UpdateTopicStatusCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CourseAggregateService,
  ) {}
  async execute(command: UpdateTopicStatusCommand) {
    const { updateTopicStatusPayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateTopicStatus(updateTopicStatusPayload, req);
    aggregate.commit();
  }
}
