import { IEvent } from '@nestjs/cqrs';
import { Course } from '../../entity/course/course.entity';

export class CourseStatusUpdatedEvent implements IEvent {
  constructor(public updateStatusPayload: Course) {}
}
