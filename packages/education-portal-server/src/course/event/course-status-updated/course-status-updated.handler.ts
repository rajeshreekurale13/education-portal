import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseService } from '../../entity/course/course.service';
import { CourseStatusUpdatedEvent } from './course-status-updated.event';

@EventsHandler(CourseStatusUpdatedEvent)
export class CourseStatusUpdatedEventHandler
  implements IEventHandler<CourseStatusUpdatedEvent> {
  constructor(private readonly object: CourseService) {}

  async handle(event: CourseStatusUpdatedEvent) {
    const { updateStatusPayload } = event;
    await this.object.updateOne(
      { course_name: updateStatusPayload.course_name },
      { $set: updateStatusPayload },
    );
  }
}
