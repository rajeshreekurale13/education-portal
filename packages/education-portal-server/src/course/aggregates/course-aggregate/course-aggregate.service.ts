import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { CourseDto } from '../../entity/course/course-dto';
import { Course } from '../../entity/course/course.entity';
import { CourseAddedEvent } from '../../event/course-added/course-added.event';
import { CourseService } from '../../entity/course/course.service';
import { CourseRemovedEvent } from '../../event/course-removed/course-removed.event';
import { CourseUpdatedEvent } from '../../event/course-updated/course-updated.event';
import { UpdateCourseDto } from '../../entity/course/update-course-dto';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';
import { UpdateTopicStatusDto } from '../../../course/entity/course/update-topic-status-dto';
import { CourseStatusUpdatedEvent } from '../../../course/event/course-status-updated/course-status-updated.event';
import { TOPICS_STATUS_ENUM } from '../../../constants/app-strings';
import {
  TOPIC_ALREADY_IN_PROGRESS,
  TOPIC_ALREADY_COMPLETED,
} from '../../../constants/messages';

@Injectable()
export class CourseAggregateService extends AggregateRoot {
  constructor(
    private readonly courseService: CourseService,
    private readonly teacherService: TeacherService,
  ) {
    super();
  }

  addCourse(coursePayload: CourseDto, clientHttpRequest) {
    const course = new Course();
    Object.assign(course, coursePayload);
    course.uuid = uuidv4();
    this.apply(new CourseAddedEvent(course, clientHttpRequest));
  }

  async retrieveCourse(getCoursePayload: GetCourseQueryDto, clientHttpRequest) {
    return this.teacherService
      .asyncAggregate([
        {
          $match: { instructor_email: clientHttpRequest.token.email },
        },
        { $unwind: '$instructor_log' },
        {
          $lookup: {
            from: 'course',
            localField: 'instructor_log.course',
            foreignField: 'name',
            as: 'topics',
          },
        },
        { $unwind: '$topics' },
        {
          $match: {
            'instructor_log.program': getCoursePayload.program_name,
            'topics.course_name': getCoursePayload.course_name,
          },
        },
        {
          $project: {
            instructor_name: 1,
            program: '$instructor_log.program',
            course_name: '$topics.course_name',
            topics: '$topics.topics',
          },
        },
      ])
      .pipe(
        switchMap((course: any[]) => {
          if (!course || !course.length) {
            return throwError(new NotFoundException());
          }
          return of(course);
        }),
      );
  }

  async getCourseList(
    offset: number,
    limit: number,
    sort: number,
    search: string,
    clientHttpRequest,
  ) {
    return this.teacherService
      .asyncAggregate([
        {
          $match: { instructor_email: clientHttpRequest.token.email },
        },
        { $unwind: '$instructor_log' },
        {
          $lookup: {
            from: 'course',
            localField: 'instructor_log.course',
            foreignField: 'name',
            as: 'topics',
          },
        },
        { $unwind: '$topics' },
        {
          $project: {
            instructor_name: 1,
            program: '$instructor_log.program',
            course_name: '$topics.course_name',
            topics: '$topics.topics',
          },
        },
        { $limit: limit },
        { $skip: offset },
        { $sort: { course_name: sort } },
      ])
      .pipe(
        switchMap((course: any[]) => {
          if (!course || !course.length) {
            return throwError(new NotFoundException());
          }
          return of(course);
        }),
      );
  }
  async remove(uuid: string) {
    const found = await this.courseService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new CourseRemovedEvent(found));
  }
  async update(updatePayload: UpdateCourseDto) {
    const provider = await this.courseService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new CourseUpdatedEvent(update));
  }

  async updateTopicStatus(updateTopicsPayload: UpdateTopicStatusDto, req) {
    const course = await this.courseService.findOne({
      course_name: updateTopicsPayload.course_name,
    });
    if (!course) {
      throw new NotFoundException();
    }
    const topicMap = {};
    course.topics.forEach(topic => {
      topicMap[topic.topic_name] = topic;
    });
    if (
      topicMap[updateTopicsPayload.topic_name].status ===
      TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS
    ) {
      topicMap[updateTopicsPayload.topic_name].status =
        TOPICS_STATUS_ENUM.COMPLETE_STATUS;
      topicMap[updateTopicsPayload.topic_name].completed_date = new Date();
    } else if (
      topicMap[updateTopicsPayload.topic_name].status ===
      TOPICS_STATUS_ENUM.REMAINING_STATUS
    ) {
      for (const key of Object.keys(topicMap)) {
        if (topicMap[key].status === TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS) {
          throw new BadRequestException(TOPIC_ALREADY_IN_PROGRESS);
        }
      }
      topicMap[updateTopicsPayload.topic_name].status =
        TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS;
    } else {
      throw new BadRequestException(TOPIC_ALREADY_COMPLETED);
    }
    course.topics = [];
    for (const keys of Object.keys(topicMap)) {
      course.topics.push(topicMap[keys]);
    }
    this.apply(new CourseStatusUpdatedEvent(course));
  }
}
