import { Test, TestingModule } from '@nestjs/testing';
import { CourseWebhookController } from './course-webhook.controller';
import { CourseWebhookAggregateService } from '../../aggregates/course-webhook-aggregate/course-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('CourseWebhook Controller', () => {
  let controller: CourseWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CourseWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [CourseWebhookController],
    }).compile();

    controller = module.get<CourseWebhookController>(CourseWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
