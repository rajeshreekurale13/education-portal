import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Program extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  program_name: string;

  @Column()
  department: string;

  @Column()
  is_published: number;

  @Column()
  allow_self_enroll: number;

  @Column()
  is_featured: number;

  @Column()
  doctype: string;

  @Column()
  courses: Course[];

  @Column()
  isSynced: boolean;
}

export class Course {
  name: string;
  docstatus: number;
  course: string;
  course_name: string;
  required: number;
  doctype: string;
}
