import { AddProgramCommandHandler } from './add-program/add-program.handler';
import { RemoveProgramCommandHandler } from './remove-program/remove-program.handler';
import { UpdateProgramCommandHandler } from './update-program/update-program.handler';

export const ProgramCommandManager = [
  AddProgramCommandHandler,
  RemoveProgramCommandHandler,
  UpdateProgramCommandHandler,
];
