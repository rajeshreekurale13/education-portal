import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ProgramWebhookDto } from '../../entity/program/program-webhook-dto';
import { ProgramWebhookAggregateService } from '../../aggregates/program-webhook-aggregate/program-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('program')
export class ProgramWebhookController {
  constructor(
    private readonly programWebhookAggreagte: ProgramWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  programCreated(@Body() programPayload: ProgramWebhookDto) {
    return this.programWebhookAggreagte.programCreated(programPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  programUpdated(@Body() programPayload: ProgramWebhookDto) {
    return this.programWebhookAggreagte.programUpdated(programPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  programDeleted(@Body() programPayload: ProgramWebhookDto) {
    return this.programWebhookAggreagte.programDeleted(programPayload);
  }
}
