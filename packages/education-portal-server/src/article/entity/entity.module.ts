import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Article } from './article/article.entity';
import { ArticleService } from './article/article.service';
import { CqrsModule } from '@nestjs/cqrs';
import { TeacherModule } from '../../teacher/teacher.module';

@Module({
  imports: [TypeOrmModule.forFeature([Article]), CqrsModule, TeacherModule],
  providers: [ArticleService],
  exports: [ArticleService],
})
export class ArticleEntitiesModule {}
