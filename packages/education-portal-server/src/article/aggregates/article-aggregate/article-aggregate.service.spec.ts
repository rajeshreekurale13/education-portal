import { Test, TestingModule } from '@nestjs/testing';
import { ArticleAggregateService } from './article-aggregate.service';
import { ArticleService } from '../../entity/article/article.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';

describe('ArticleAggregateService', () => {
  let service: ArticleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
        {
          provide: TeacherService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleAggregateService>(ArticleAggregateService);
  });
  ArticleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
