import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { ArticleDto } from '../../entity/article/article-dto';
import { Article } from '../../entity/article/article.entity';
import { ArticleAddedEvent } from '../../event/article-added/article-added.event';
import { ArticleService } from '../../entity/article/article.service';
import { ArticleRemovedEvent } from '../../event/article-removed/article-removed.event';
import { ArticleUpdatedEvent } from '../../event/article-updated/article-updated.event';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

@Injectable()
export class ArticleAggregateService extends AggregateRoot {
  constructor(
    private readonly articleService: ArticleService,
    private readonly teacherService: TeacherService,
  ) {
    super();
  }

  addArticle(articlePayload: ArticleDto, clientHttpRequest) {
    const article = new Article();
    Object.assign(article, articlePayload);
    article.uuid = uuidv4();
    this.apply(new ArticleAddedEvent(article, clientHttpRequest));
  }

  async retrieveArticle(uuid: string, req) {
    const provider = await this.articleService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getArticleList(
    offset,
    limit,
    sort,
    search,
    clientHttpRequest,
    getArticleList: GetCourseQueryDto,
  ) {
    return this.teacherService
      .asyncAggregate([
        {
          $match: { instructor_email: clientHttpRequest.token.email },
        },
        {
          $unwind: '$instructor_log',
        },
        {
          $match: {
            'instructor_log.program': getArticleList.program_name,
            'instructor_log.course': getArticleList.course_name,
          },
        },
        {
          $lookup: {
            from: 'course',
            let: { course: '$instructor_log.course' },
            pipeline: [
              {
                $match: { course_name: getArticleList.course_name },
              },
              {
                $match: {
                  $expr: { $eq: ['$course_name', '$$course'] },
                },
              },
              {
                $unwind: '$topics',
              },
              {
                $lookup: {
                  from: 'topic',
                  let: { topic: '$topics.topic_name' },
                  pipeline: [
                    {
                      $match: {
                        $expr: { $eq: ['$topic_name', '$$topic'] },
                      },
                    },
                    { $unwind: '$topic_content' },
                    {
                      $lookup: {
                        from: 'article',
                        let: { one_article: '$topic_content.content' },
                        pipeline: [
                          {
                            $match: {
                              $expr: { $eq: ['$name', '$$one_article'] },
                            },
                          },
                        ],
                        as: 'aggregate_article',
                      },
                    },
                    { $unwind: '$aggregate_article' },
                  ],
                  as: 'aggregate_topics',
                },
              },
              { $unwind: '$aggregate_topics' },
            ],
            as: 'aggregate_course',
          },
        },
        { $unwind: '$aggregate_course' },
        {
          $project: {
            title: '$aggregate_course.aggregate_topics.aggregate_article.title',
            type:
              '$aggregate_course.aggregate_topics.aggregate_article.doctype',
            content:
              '$aggregate_course.aggregate_topics.aggregate_article.content',
          },
        },
      ])
      .pipe(
        switchMap((article: any[]) => {
          if (!article || !article.length) {
            return throwError(new NotFoundException());
          }
          return of(article);
        }),
      );
  }

  async remove(uuid: string) {
    const found = await this.articleService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new ArticleRemovedEvent(found));
  }

  async update(updatePayload: UpdateArticleDto) {
    const provider = await this.articleService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new ArticleUpdatedEvent(update));
  }
}
