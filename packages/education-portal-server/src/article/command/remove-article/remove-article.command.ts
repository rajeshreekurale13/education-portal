import { ICommand } from '@nestjs/cqrs';

export class RemoveArticleCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
