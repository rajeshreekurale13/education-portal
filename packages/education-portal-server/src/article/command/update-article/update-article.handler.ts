import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateArticleCommand } from './update-article.command';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@CommandHandler(UpdateArticleCommand)
export class UpdateArticleCommandHandler
  implements ICommandHandler<UpdateArticleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ArticleAggregateService,
  ) {}

  async execute(command: UpdateArticleCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
