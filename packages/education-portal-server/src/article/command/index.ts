import { AddArticleCommandHandler } from './add-article/add-article.handler';
import { RemoveArticleCommandHandler } from './remove-article/remove-article.handler';
import { UpdateArticleCommandHandler } from './update-article/update-article.handler';

export const ArticleCommandManager = [
  AddArticleCommandHandler,
  RemoveArticleCommandHandler,
  UpdateArticleCommandHandler,
];
