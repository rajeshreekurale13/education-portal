import { IQuery } from '@nestjs/cqrs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

export class RetrieveArticleListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public clientHttpRequest: any,
    public getArticleList: GetCourseQueryDto,
  ) {}
}
