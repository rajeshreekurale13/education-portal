import { Module, HttpModule } from '@nestjs/common';
import { ArticleAggregatesManager } from './aggregates';
import { ArticleEntitiesModule } from './entity/entity.module';
import { ArticleQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { ArticleCommandManager } from './command';
import { ArticleEventManager } from './event';
import { ArticleController } from './controllers/article/article.controller';
import { ArticlePoliciesService } from './policies/article-policies/article-policies.service';
import { ArticleWebhookController } from './controllers/article-webhook/article-webhook.controller';
import { TeacherModule } from '../teacher/teacher.module';

@Module({
  imports: [ArticleEntitiesModule, CqrsModule, HttpModule, TeacherModule],
  controllers: [ArticleController, ArticleWebhookController],
  providers: [
    ...ArticleAggregatesManager,
    ...ArticleQueryManager,
    ...ArticleEventManager,
    ...ArticleCommandManager,
    ArticlePoliciesService,
  ],
  exports: [ArticleEntitiesModule],
})
export class ArticleModule {}
